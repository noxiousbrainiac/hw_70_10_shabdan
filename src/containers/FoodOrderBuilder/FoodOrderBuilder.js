import React, {useEffect, useState} from 'react';
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {axiosMenu, calculateItem, calculateToRemove, createOrder} from "../../store/actions";
import {Container} from "@material-ui/core";
import FoodMenu from "../../components/FoodMenu/FoodMenu";
import Basket from "../../components/Basket/Basket";
import ModalContent from "../../components/UI/ModalContent/ModalContent";

const FoodOrderBuilder = () => {
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);

    const [inputHandle, setInputHandle] = useState({
        address: "",
        name: "",
        number: ""
    });

    const {orders, menu, totalPrice} = useSelector(state => ({
        orders: state.orders,
        menu:state.menu,
        totalPrice: state.totalPrice,
    }), shallowEqual);

    useEffect(() => {
        dispatch(axiosMenu());
    },[dispatch]);

    const addToCart = (dish) => {
        dispatch(calculateItem(dish, orders));
    }

    const removeOrder = (dish) => {
        dispatch(calculateToRemove(dish, orders, menu));
    }

    const onChangeHandle = (e) => {
        const {name, value} = e.target;
        setInputHandle(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const create = () => {
        if (inputHandle.address.length > 0) {
            const newOrder = {
                order: orders,
                info: inputHandle
            }
            dispatch(createOrder(newOrder));
        }
        setOpen(false);

    }

    const placeOrder = () => {
        setOpen(true);
    }

    const close = () => {
        setOpen(false);
    }

    return (
        <Container>
            <div style={{display: "flex", justifyContent: "space-evenly"}}>
                <FoodMenu menu={menu} onClick={addToCart}/>
                <Basket onClick={removeOrder} order={orders} showModal={placeOrder} totalPrice={totalPrice}/>
            </div>
            <ModalContent open={open} close={close} inputHandle={inputHandle} onChangeHandle={onChangeHandle} create={create}/>
        </Container>
    );
};

export default FoodOrderBuilder;