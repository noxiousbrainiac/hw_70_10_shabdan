import axiosApi from "../axiosApi";

export const GET_MENU = "GET_MENU";
export const ADD_DISH = "ADD_DISH";
export const REMOVE_DISH = "REMOVE_DISH";
export const POST_ORDER = "POST_ORDER";

export const getMenu = payload => ({type: GET_MENU, payload});
export const addDish = (dish, price) => ({type: ADD_DISH, payload: dish, price});
export const removeDish = dish => ({type: REMOVE_DISH, payload : dish});
export const postOrder = payload => ({type: POST_ORDER, payload});

export const axiosMenu = () => async (dispatch) => {
    try {
        const {data} = await axiosApi.get("/dishes.json");
        dispatch(getMenu(data));
    } catch (e) {
        console.error(e);
    }
}

export const calculateItem = (obj, array) => (dispatch) => {
    const baseArray = [...array];
    if (array.findIndex(item => item.name === obj.name) === -1) {
        baseArray.push({
            name: obj.name,
            amount: 1,
            price: obj.price
        })
    } else {
        const newArray = array.map(item => {
            if (item.name === obj.name) {
                item.amount++;
                item.price+= obj.price;
            }

            return item;
        })

        dispatch(addDish(newArray, obj.price));
    }
    dispatch(addDish(baseArray, obj.price));
}

export const calculateToRemove = (obj, array, menu) => (dispatch) => {
    const baseArray = [...array];
    const itemIndex = array.findIndex(item => item.name === obj.name);
    if (itemIndex !== -1 && baseArray[itemIndex].amount > 1) {
        baseArray[itemIndex].amount--;
        baseArray[itemIndex].price-=menu[baseArray[itemIndex].name].price;
    } else {
        baseArray.splice(itemIndex, 1);
    }
    dispatch(removeDish(baseArray));
}

export const createOrder = (userInfo) => async () => {
    try {
        await axiosApi.post("/toDelivery.json", userInfo);
    } catch (e) {
        console.log(e);
    }
}




