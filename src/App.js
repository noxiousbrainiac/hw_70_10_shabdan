import React from 'react';
import FoodOrderBuilder from "./containers/FoodOrderBuilder/FoodOrderBuilder";

const App = () => {
    return (
        <div>
          <FoodOrderBuilder/>
        </div>
    );
};

export default App;