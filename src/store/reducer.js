import {ADD_DISH, GET_MENU, REMOVE_DISH} from "./actions";

const initialState = {
    orders: [],
    menu: [],
    totalPrice: 150,
    open: false,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MENU:
            return {...state, menu: action.payload}
        case ADD_DISH:
        case REMOVE_DISH:
            console.log(action.price)
            return {
                ...state,
                orders: action.payload,
                totalPrice: state.totalPrice + action.price
            }
        default:
            return state;
    }
}

export default reducer;