import React from 'react';
import {Button, Card} from "@material-ui/core";

const Basket = ({order, onClick , totalPrice, showModal}) => {
    return (
        <Card style={{width: "200px", padding: "5px", background: "gray"}}>
            <ul style={{padding: "0"}}>
                {order?.map(item => (
                    <li key={item.name} style={{
                        display: "flex",
                        justifyContent: "space-evenly",
                        alignItems: "center",
                        marginBottom: "5px"
                    }}>
                        <span style={{textTransform: "capitalize", flexGrow: "1"}}>{item?.name}</span>
                        <b style={{flexGrow: "1"}}>{item?.amount}x</b>
                        <span style={{flexGrow: "1"}}>{item?.price}</span>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => onClick(item)}
                        >
                            X
                        </Button>
                    </li>
                ))}
            </ul>
            <div>Delivery: <b>150</b></div>
            <div>Total price: <b>{totalPrice}</b></div>
            <Button
                variant="contained"
                color="secondary"
                onClick={() => showModal()}
            >
                Place order
            </Button>
        </Card>
    );
};

export default Basket;