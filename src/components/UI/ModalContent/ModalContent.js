import React from 'react';
import {Button, Card, Modal, TextField} from "@material-ui/core";

const ModalContent = ({close, open ,create, onChangeHandle, inputHandle}) => {
    return (
        <Modal
            open={open}
            onClose={() => close()}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Card style={{
                position: "absolute",
                top: "100px",
                left: "50%",
                transform: "translateX(-50%)",
                width: "500px",
                padding: "10px",
                display: "flex",
                flexDirection: "column"
            }}>
                <div style={{display: "flex", flexDirection: "column"}}>
                    <TextField
                        style={{marginBottom: "10px"}}
                        label="Name"
                        name="name"
                        variant="outlined"
                        size={"small"}
                        value={inputHandle.name}
                        onChange={onChangeHandle}/>
                    <TextField
                        style={{marginBottom: "10px"}}
                        label="Phone number"
                        name="number"
                        variant="outlined"
                        size={"small"}
                        value={inputHandle.number}
                        onChange={onChangeHandle}/>
                    <TextField
                        style={{marginBottom: "10px"}}
                        label="Address"
                        name="address"
                        variant="outlined"
                        size={"small"}
                        value={inputHandle.address}
                        onChange={onChangeHandle}
                    />
                </div>
                <Button
                    style={{alignSelf: "center"}}
                    color="secondary"
                    variant="contained"
                    onClick={() => create()}
                >
                    Create order
                </Button>
            </Card>
        </Modal>
    );
};

export default ModalContent;