import React from 'react';
import {Button, Card} from "@material-ui/core";

const FoodMenu = ({menu, onClick}) => {
    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            {Object.keys(menu).map(dish => (
                <Card
                    key={dish}
                    style={{
                        backgroundColor: "gray",
                        width: "320px",
                        marginTop: "10px",
                        display: "flex",
                        justifyContent: "space-evenly",
                        alignItems: "center"
                    }}
                >
                    <div style={{width: "50px"}}>
                        <img
                            src={menu[dish]?.image}
                            style={{width: "100%"}}
                            alt="dish"
                        />
                    </div>
                    <div>
                        <h4 style={{textTransform: "capitalize"}}>{menu[dish]?.name}</h4>
                        <h4>{menu[dish]?.price}</h4>
                    </div>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => onClick(menu[dish])}
                    >
                        Add to card
                    </Button>
                </Card>
            ))}
        </div>
    );
};

export default FoodMenu;